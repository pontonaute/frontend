import React from 'react';
import { Link } from 'react-router-dom';
import { Menu } from 'semantic-ui-react';

/**
 * Barre de navigation du site
 */
export class Navbar extends React.Component {
  constructor(props) {
    super();
    this.state = { activeItem: 'home' };
  }
  componentDidMount() {
    switch (window.location.pathname) {
      case '/inscription':
        this.setState({
          activeItem: 'register',
        });
        break;
      case '/':
        this.setState({
          activeItem: 'home',
        });
        break;
      default:
        this.setState({
          activeItem: '',
        });
        break;
    }
  }

  handleItemClick = (e, { name }) => this.setState({ activeItem: name });

  render() {
    const { activeItem } = this.state;
    return (
      <div id="navbar">
        <Menu size="massive" stackable>
          <Link to="/">
            <Menu.Item
              as="span"
              name="home"
              active={activeItem === 'home'}
              content="Accueil"
              onClick={this.handleItemClick}
            />
          </Link>

          <Link to="/inscription">
            <Menu.Item
              as="span"
              name="register"
              active={activeItem === 'register'}
              content="S'inscrire"
              onClick={this.handleItemClick}
            />
          </Link>
        </Menu>
      </div>
    );
  }
}
