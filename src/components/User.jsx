import React from 'react';
import { Label, Icon, List, Segment } from 'semantic-ui-react';
import { Link } from 'react-router-dom';
import ReactHtmlParser from 'react-html-parser';
import { dateConverter } from '../utils/DateConverter';

/**
 * Informations d'un utilisateur inscrit dans la base de données
 */
export class User extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isClicked: false,
    };
    this.handleClick = this.handleClick.bind(this);
    this.myRef = React.createRef();
  }

  handleClick = () => {
    this.setState({
      isClicked: this.state.isClicked ? false : true,
    });
  };

  // retourner au début quand on click sur la flèche pour masquer les infos supplémentaires
  scrollToMyRef = () => {
    if (this.myRef.current)
      this.myRef.current.scrollIntoView({
        block: 'end',
        behavior: 'smooth',
      });
    //
    this.setState({
      isClicked: false,
    });
  };

  // afficher plus d'infos
  firstIconRender() {
    if (this.state.isClicked) {
      return (
        <Icon size="big" name="arrow circle down" onClick={this.handleClick} />
      );
    } else {
      return (
        <Icon size="big" name="arrow circle right" onClick={this.handleClick} />
      );
    }
  }
  // masquer les infos affichées
  lastIconRender() {
    if (this.state.isClicked) {
      return (
        <Icon size="big" name="arrow circle up" onClick={this.scrollToMyRef} />
      );
    }
  }

  horairesRender(jour) {
    let heures = '';
    let estOuvert = true;
    let compteur = 0; // compteur les jours vides
    jour.forEach((item, index) => {
      if (item !== '') {
        if (index !== 1 && index !== 3)
          heures += item + ReactHtmlParser(' &#x2501; ');
        else if (index === 1) heures += item + ReactHtmlParser('&emsp;');
        else heures += item;
      } else {
        compteur++;
      }

      if (compteur === 4) {
        compteur = 0; // remise à 0
        estOuvert = false;
      }
    });

    return estOuvert ? heures : 'fermé';
  }

  livraisonRender(livraison_comment) {
    let comment = '';

    livraison_comment.forEach((item, index) => {
      if (item !== ('' || null || undefined)) {
        if (index !== livraison_comment.length - 1) comment += item + ', ';
        else {
          comment += item;
        }
      }
    });
    return comment;
  }

  moyenVenteRender(moyen) {
    let moyen_de_vente = '';
    if (moyen.length > 0) {
      moyen.forEach((item, index) => {
        if (item !== ('' || null || undefined)) {
          if (index !== moyen.length - 1) moyen_de_vente += item + ', ';
          else {
            moyen_de_vente += item;
          }
        }
      });
      return moyen_de_vente;
    } else {
      return "Pas d'information.";
    }
  }

  moyenVentePrecisionRender(moyen_precision) {
    if (moyen_precision.length > 0) {
      return moyen_precision;
    } else {
      return 'Pas de précision.';
    }
  }

  moyenPaiementRender(moyen) {
    let moyen_paiement = '';
    if (moyen.length > 0) {
      moyen.forEach((item, index) => {
        if (item !== ('' || null || undefined)) {
          if (index !== moyen.length - 1) moyen_paiement += item + ', ';
          else {
            moyen_paiement += item;
          }
        }
      });
      return moyen_paiement;
    } else {
      return "Pas d'information.";
    }
  }

  informationComplementaireRender(info) {
    if (info.length > 0) {
      return info;
    } else {
      return "Pas d'information complémentaire.";
    }
  }

  descriptionRender() {
    const data = this.props.data;

    return (
      <div className="infos-supplementaire" hidden={!this.state.isClicked}>
        <List size="big">
          <List.Item>
            <List.Icon
              name="user outline"
              size="large"
              verticalAlign="bottom"
            />
            <List.Content>
              <List.Description>{data.jesuis}</List.Description>
            </List.Content>
          </List.Item>

          <List.Item>
            <List.Icon
              name="comment outline"
              size="large"
              verticalAlign="bottom"
            />
            <List.Content>
              <List.Description>{data.description}</List.Description>
            </List.Content>
          </List.Item>

          <List.Item>
            <List.Icon
              name="certificate"
              color="green"
              size="large"
              verticalAlign="middle"
            />
            <List.Content>
              <List.Header content="Label" />
              <List.Description>{data.label}</List.Description>
            </List.Content>
          </List.Item>

          <List.Item>
            <List.Icon
              color="red"
              name="map pin"
              size="large"
              verticalAlign="middle"
            />
            <List.Content>
              <List.Header content="Localisation" />
              <List.Description>
                {data.adresse + ', ' + data.ville + ', ' + data.code_postal}
              </List.Description>
            </List.Content>
          </List.Item>

          <List.Item>
            <List.Content>
              <List.Header content="Réseaux" />
              <List.Description>
                <Icon color="blue" name="facebook" />{' '}
                <a
                  href={data.facebook}
                  target="_blank"
                  rel="noopener noreferrer"
                >
                  {data.facebook}
                </a>
              </List.Description>
              <List.Description>
                <Icon name="linkify" />{' '}
                <a href={data.site} target="_blank" rel="noopener noreferrer">
                  {data.site}
                </a>
              </List.Description>
            </List.Content>
          </List.Item>

          <List.Item>
            <List.Content>
              <List.Header content="Horaires" />
              <List.Description>
                <div className="jours">Lundi : </div>
                <div className="horaires">
                  {this.horairesRender(data.horaires.lundi)}
                </div>
              </List.Description>
              <List.Description>
                <div className="jours">Mardi : </div>
                <div className="horaires">
                  {this.horairesRender(data.horaires.mardi)}
                </div>
              </List.Description>
              <List.Description>
                <div className="jours">Mercredi : </div>
                <div className="horaires">
                  {this.horairesRender(data.horaires.mercredi)}
                </div>
              </List.Description>
              <List.Description>
                <div className="jours">Jeudi : </div>
                <div className="horaires">
                  {this.horairesRender(data.horaires.jeudi)}
                </div>
              </List.Description>
              <List.Description>
                <div className="jours">Vendredi: </div>
                <div className="horaires">
                  {this.horairesRender(data.horaires.vendredi)}
                </div>
              </List.Description>
              <List.Description>
                <div className="jours">Samedi : </div>
                <div className="horaires">
                  {this.horairesRender(data.horaires.samedi)}
                </div>
              </List.Description>
              <List.Description>
                <div className="jours">Dimanche : </div>
                <div className="horaires">
                  {this.horairesRender(data.horaires.dimanche)}
                </div>
              </List.Description>
            </List.Content>
          </List.Item>

          <List.Item>
            <List.Content>
              <List.Header content="Livraison" />
              {(() => {
                if (data.livraison.livraison_possible) {
                  return (
                    <div>
                      <List.Description>
                        <Label
                          size="large"
                          color="orange"
                          content="Comment sont livrés les produits ?"
                        />{' '}
                        &ensp; {this.livraisonRender(data.livraison.comment)}
                      </List.Description>

                      <List.Description>
                        <Label
                          size="large"
                          color="orange"
                          content="Panier minimum pour être livré"
                        />{' '}
                        &ensp;{' '}
                        {ReactHtmlParser(
                          data.livraison.panier_minimum + ' &euro;'
                        )}
                      </List.Description>

                      <List.Description>
                        <Label
                          size="large"
                          color="orange"
                          content="Précision concernant la livraison"
                        />{' '}
                        &ensp;{' '}
                        {(() => {
                          return data.livraison.comment_precision !== ''
                            ? data.livraison.comment_precision
                            : 'Pas de précisions.';
                        })()}
                      </List.Description>
                    </div>
                  );
                } else {
                  return (
                    <List.Description>
                      <Label
                        size="large"
                        color="blue"
                        content="Comment sont livrés les produits ?"
                      />{' '}
                      &ensp; Pas de livraison.
                    </List.Description>
                  );
                }
              })()}
            </List.Content>
          </List.Item>

          <List.Item>
            <List.Content>
              <List.Header content="Moyens de vente" />
              <List.Description>
                <Label color="olive" size="large" content="Moyens" />
                &ensp;
                {this.moyenVenteRender(data.moyen_de_vente.moyen)}
              </List.Description>
              <List.Description>
                <Label color="olive" size="large" content="Précision" />
                &ensp;
                {this.moyenVentePrecisionRender(data.moyen_de_vente.precision)}
              </List.Description>
            </List.Content>
          </List.Item>

          <List.Item>
            <List.Content>
              <List.Header content="Moyens de paiement" />
              <List.Description>
                <Label color="grey" size="large" content="Moyens" />
                &ensp;
                {this.moyenPaiementRender(data.moyen_paiement)}
              </List.Description>
            </List.Content>
          </List.Item>

          <List.Item>
            <List.Content>
              <List.Header content="Information complémentaire" />
              <List.Description>
                <Icon color="red" size="large" name="info circle" />
                &ensp;
                {this.informationComplementaireRender(
                  data.information_complementaire
                )}
              </List.Description>
            </List.Content>
          </List.Item>

          <List.Item>
            <List.Content>
              <List.Header content="Inscription" />
              <List.Description>
                <Label color="pink" size="large" content="Date" />
                &ensp;
                {dateConverter(data.horodateur)}
              </List.Description>
            </List.Content>
          </List.Item>

          <List.Content
            verticalAlign="middle"
            floated="right"
            style={{ textAlign: 'right' }}
          >
            {this.lastIconRender()}
          </List.Content>
        </List>
      </div>
    );
  }

  render() {
    const data = this.props.data;
    return (
      <div className="user" ref={this.myRef}>
        <Segment>
          <List size="large">
            <List.Item>
              <List.Content verticalAlign="middle" floated="right">
                {this.firstIconRender()}
              </List.Content>
              <List.Content>
                <Link to={'/' + data._id}>{data.nom + ' ' + data.prenom}</Link>
                <List.Description>
                  <Label size="large" color="blue" content="Commerce" />
                  &ensp; {data.nom_commerce}
                </List.Description>
                <List.Description>
                  <Label size="large" color="blue" content="Activité" />
                  &ensp;
                  {data.activite}
                </List.Description>
                <List.Description>
                  <Label size="large" color="blue" content="Ouvert ?" />
                  &emsp;
                  {(() => {
                    return data.est_ouvert ? (
                      <Icon size="large" name="check" color="green" />
                    ) : (
                      <Icon size="large" name="times" color="red" />
                    );
                  })()}
                </List.Description>
                <List.Description>
                  <Icon name="mail" />{' '}
                  <a href={'mailto:' + data.courriel}>{data.courriel}</a>
                </List.Description>
                <List.Description>
                  <Icon name="phone" />{' '}
                  <a href={'tel:' + data.tel_mobile}>{data.tel_mobile}</a>
                </List.Description>
                <List.Description>
                  <Icon name="text telephone" />{' '}
                  <a href={'tel:' + data.tel_fixe}>{data.tel_fixe}</a>
                </List.Description>
              </List.Content>
            </List.Item>
          </List>
          {this.descriptionRender()}
        </Segment>
      </div>
    );
  }
}
