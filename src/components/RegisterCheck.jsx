import React from 'react';
import { Header, Icon } from 'semantic-ui-react';
export const RegisterCheck = () => {
  return (
    <div id="inscription-validee">
      <Header as="h2" icon textAlign="center">
        <Icon name="check" color="green" circular />
        <Header.Content>Inscription validée !</Header.Content>
      </Header>
    </div>
  );
};
