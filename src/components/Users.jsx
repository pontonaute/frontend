import React from 'react';
import { API } from '../utils/API';
import { Header, Icon } from 'semantic-ui-react';
import { User } from '../components/User';

/**
 * Lister tous les utilisateurs de la base de données par un cadre rectangulaire
 * Les informations de ces utilisateurs sont dans User.jsx
 */
export class Users extends React.Component {
  constructor(props) {
    super();
    this.state = {
      data: [],
    };
  }

  componentDidMount() {
    API.getUsers()
      .then(response => {
        if (response.status === 200) {
          response.data.forEach(item => {
            this.setState({
              data: [...this.state.data, item],
            });
          });
        }
      })
      .catch(error => {
        // si aucun utilisateur en BDD, le compteur des inscrits sera à 0 donc pas nécessaire d'informer l'utilisateur
        if (String(error) !== 'Error: Request failed with status code 404') {
          alert(error);
          console.log(error);
        }
      });
  }

  usersRender() {
    let users = [];
    this.state.data.forEach(item => {
      users.push(<User key={item._id} data={item} />);
    });

    return users;
  }

  render() {
    return (
      <div id="users">
        <Header as="h3" icon textAlign="center">
          <Icon circular>{this.state.data.length}</Icon>
          <Header.Content>Utilisateurs inscrits</Header.Content>
        </Header>
        {this.usersRender()}
      </div>
    );
  }
}
