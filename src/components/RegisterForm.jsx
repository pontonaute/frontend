import React from 'react';
import {
  Button,
  Form,
  Dimmer,
  Loader,
  Message,
  Label,
  Icon,
} from 'semantic-ui-react';
import { API } from '../utils/API';
import $ from 'jquery';
import { Redirect } from 'react-router-dom';

// options du <Select /> pour les choix d'activités possible
const optionsActivite = [
  {
    key: 'flf',
    text: 'Fruits et légumes de la ferme',
    value: 'Fruits et légumes de la ferme',
  },
  {
    key: 'plof',
    text: 'Produits laitiers et oeufs de ferme',
    value: 'Produits laitiers et oeufs de ferme',
  },
  {
    key: 'vpf',
    text: 'Viande produite à la ferme',
    value: 'Viande produite à la ferme',
  },
  { key: 'b', text: 'Boissons', value: 'Boissons' },
  {
    key: 'bpc',
    text: 'Boulangerie-Pâtisserie-Chocolaterie',
    value: 'Boulangerie-Pâtisserie-Chocolaterie',
  },
  { key: 'p', text: 'Poissonerie', value: 'Poissonerie' },
  { key: 'e', text: 'Epicerie', value: 'Epicerie' },
  { key: 'bo', text: 'Boucherie', value: 'Boucherie' },
  { key: 'bes', text: 'Bien-être et santé', value: 'Bien-être et santé' },
  { key: 'lj', text: 'Loisir-Jardinage', value: 'Loisir-Jardinage' },
  { key: 'h', text: 'Habillement', value: 'Habillement' },
  { key: 'tp', text: 'Tabac-Presse', value: 'Tabac-Presse' },
  { key: 'eq', text: 'Equipement', value: 'Equipement' },
  {
    key: 'pa',
    text: 'Produits pour les animaux',
    value: 'Produits pour les animaux',
  },
  { key: 'pe', text: 'Plats à emporter', value: 'Plats à emporter' },
  { key: 'f', text: 'Fromagerie', value: 'Fromagerie' },
  { key: 'a', text: 'Autres', value: 'Autres' },
];

/**
 * Enregistrement d'un utilisateur en base de données
 */
export class RegisterForm extends React.Component {
  constructor(props) {
    super();
    this.state = {
      loader: false,
      redirect: false,
      tel_fixe: '',
      tel_mobile: '',
      nom: '',
      prenom: '',
      jesuis: '',
      nom_commerce: '',
      description: '',
      adresse: '',
      ville: '',
      code_postal: '',
      facebook: '',
      site: '',
      activite: '',
      est_ouvert: false, // ouvert ou fermé

      lundi_matin_debut: '',
      lundi_matin_fin: '',
      lundi_aprem_debut: '',
      lundi_aprem_fin: '',

      mardi_matin_debut: '',
      mardi_matin_fin: '',
      mardi_aprem_debut: '',
      mardi_aprem_fin: '',

      mercredi_matin_debut: '',
      mercredi_matin_fin: '',
      mercredi_aprem_debut: '',
      mercredi_aprem_fin: '',

      jeudi_matin_debut: '',
      jeudi_matin_fin: '',
      jeudi_aprem_debut: '',
      jeudi_aprem_fin: '',

      vendredi_matin_debut: '',
      vendredi_matin_fin: '',
      vendredi_aprem_debut: '',
      vendredi_aprem_fin: '',

      samedi_matin_debut: '',
      samedi_matin_fin: '',
      samedi_aprem_debut: '',
      samedi_aprem_fin: '',

      dimanche_matin_debut: '',
      dimanche_matin_fin: '',
      dimanche_aprem_debut: '',
      dimanche_aprem_fin: '',

      information_horaire: '', // info sur les ouvertures/fermetures

      livraison: false, // livre ou ne livre pas
      comment: [
        { key: 'envoilaposte', value: 'Envoi par la poste', isChecked: false },
        {
          key: 'livraisonvousmeme',
          value: 'Livraison effectuée par vous-même',
          isChecked: false,
        },
        {
          key: 'depotchezcommercant',
          value: 'Dépôt chez un autre commerçant (précisez où dans précision)',
          isChecked: false,
        },
        {
          key: 'livraisonautre',
          value: 'Autre (précisez dans précisions)',
          isChecked: false,
        },
      ], // comment est livré le produit
      comment_precision: '',

      panier_minimum: '', // prix minimum pour être livré

      moyen_paiement: [
        { key: 'cartebancaire', value: 'Carte bancaire', isChecked: false },
        { key: 'cheque', value: 'Chèque', isChecked: false },
        { key: 'espece', value: 'Espèce', isChecked: false },
      ], // carte bancaire, espèce, chèque
      moyen_de_vente: [
        {
          key: 'marche',
          value: 'Marché (Précisez le jour et le lieu dans précisions)',
          isChecked: false,
        },
        {
          key: 'drive',
          value: 'Drive (point de retrait à préciser dans précisions)',
          isChecked: false,
        },
        {
          key: 'boutiqueenligne',
          value:
            'Boutique en ligne (Adresse de la boutique à préciser dans précisions)',
          isChecked: false,
        },
        {
          key: 'depotcolis',
          value: 'Dépôt de colis à la poste',
          isChecked: false,
        },
        {
          key: 'plateforme',
          value:
            'Plateforme "Ma ville Mon Shopping" (Adresse de la boutique à préciser dans précisions)',
          isChecked: false,
        },
        {
          key: 'boutiqueouverte',
          value:
            'Boutique ouverte accueillant vos produits (préciser dans précisions)',
          isChecked: false,
        },
        {
          key: 'aucunautremoyen',
          value: 'Aucun autre moyen de vente',
          isChecked: false,
        },
        {
          key: 'autremoyen',
          value: 'Autre moyen de vente (à préciser dans précisions)',
          isChecked: false,
        },
      ],
      moyen_de_vente_precision: '',

      information_complementaire: '',

      image_personne: [],
      image_produit: [],

      label: '',
      autorisation_pontonaute: false,
      courriel: '',
      message: '', // message d'erreur/succès
      class: 'positive', // couleur rouge/vert
    };
    this.handleClick = this.handleClick.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleCheckGroup = this.handleCheckGroup.bind(this);
  }
  componentDidMount() {
    $('#message-info').hide(); // enlever tous les messages
  }

  showMessage() {
    $('#message-info').show('fast').delay(5000).hide('slow');
  }

  // input
  handleChange = e => {
    this.setState({
      [e.target.name]: String(e.target.value),
    });
  };
  handleCheckGroup = (e, { value, name }) => {
    switch (name) {
      case 'moyen_de_vente':
        let moyen_de_vente = this.state.moyen_de_vente;
        moyen_de_vente.forEach(item => {
          if (item.value === value)
            item.isChecked ? (item.isChecked = false) : (item.isChecked = true);
        });
        this.setState({ moyen_de_vente });
        break;
      case 'moyen_paiement':
        let moyen_paiement = this.state.moyen_paiement;
        moyen_paiement.forEach(item => {
          if (item.value === value)
            item.isChecked ? (item.isChecked = false) : (item.isChecked = true);
        });
        this.setState({ moyen_paiement });
        break;
      case 'comment':
        let comment = this.state.comment;
        comment.forEach(item => {
          if (item.value === value)
            item.isChecked ? (item.isChecked = false) : (item.isChecked = true);
        });
        this.setState({ comment });
        break;

      default:
        break;
    }
  };

  // boutton radio, checkbox, select
  handleClick = (e, { value, name }) => {
    this.setState({
      [name]: String(value),
    });
  };

  handleSubmit = e => {
    e.preventDefault();

    const _ = this.state;

    // comment se fait la livraison
    const comment = [];
    _.comment.forEach(item => {
      let str = String(item).split(' ('); // supprimer les parenthèses de précision
      if (item.isChecked) comment.push(String(str[0].value).toLowerCase());
    });

    const livraison = {};
    // si livraison est cochée on mets toutes les infos
    if (_.livraison) {
      livraison.livraison_possible = _.livraison;
      livraison.comment = comment;
      livraison.comment_precision = _.comment_precision;
      livraison.panier_minimum = _.panier_minimum;
    } else {
      // sinon on enlève tout par sécurité si l'utilisateur coche livraison puis les checkboxs puis décoche livraison
      livraison.livraison_possible = _.livraison;
    }

    // moyens de paiement
    const moyen_paiement = [];
    _.moyen_paiement.forEach(item => {
      if (item.isChecked) moyen_paiement.push(String(item.value).toLowerCase());
    });

    // moyens de vente
    const moyen_de_vente = {
      moyen: [],
      precision: _.moyen_de_vente_precision,
    };
    _.moyen_de_vente.forEach(item => {
      let str = String(item).split(' ('); // supprimer les parenthèses de précision
      if (item.isChecked)
        moyen_de_vente.moyen.push(String(str[0].value).toLowerCase());
    });

    let data = {
      nom: _.nom, //1
      prenom: _.prenom, //2
      courriel: _.courriel, //3
      jesuis: _.jesuis, //5
      tel: _.tel, //4
      nom_commerce: _.nom_commerce, //6
      description: _.description, //7
      adresse: _.adresse, //8
      ville: _.ville, //9
      code_postal: _.code_postal, //10
      facebook: _.facebook, //11
      site: _.site, //12
      activite: _.activite, //13
      est_ouvert: _.est_ouvert, //14
      horaires: {
        lundi: [
          _.lundi_matin_debut,
          _.lundi_matin_fin,
          _.lundi_aprem_debut,
          _.lundi_aprem_fin,
        ],
        mardi: [
          _.mardi_matin_debut,
          _.mardi_matin_fin,
          _.mardi_aprem_debut,
          _.mardi_aprem_fin,
        ],
        mercredi: [
          _.mercredi_matin_debut,
          _.mercredi_matin_fin,
          _.mercredi_aprem_debut,
          _.mercredi_aprem_fin,
        ],
        jeudi: [
          _.jeudi_matin_debut,
          _.jeudi_matin_fin,
          _.jeudi_aprem_debut,
          _.jeudi_aprem_fin,
        ],
        vendredi: [
          _.vendredi_matin_debut,
          _.vendredi_matin_fin,
          _.vendredi_aprem_debut,
          _.vendredi_aprem_fin,
        ],
        samedi: [
          _.samedi_matin_debut,
          _.samedi_matin_fin,
          _.samedi_aprem_debut,
          _.samedi_aprem_fin,
        ],
        dimanche: [
          _.dimanche_matin_debut,
          _.dimanche_matin_fin,
          _.dimanche_aprem_debut,
          _.dimanche_aprem_fin,
        ],

        information_horaire: _.information_horaire,
      }, //15
      livraison, //16
      moyen_paiement, //17
      moyen_de_vente, //18
      information_complementaire: _.information_complementaire, //19
      label: _.label, //20
      autorisation_pontonaute: _.autorisation_pontonaute, //21
    };

    if (_.autorisation_pontonaute) {
      this.setState({ loader: true }); // afficher le loader en attendant la réponse du serveur
      API.register(data)
        .then(response => {
          this.setState({ loader: false });

          if (response.status === 201) {
            this.setState({
              redirect: true,
            });
          }
        })
        .catch(err => {
          console.log(err);
          this.setState({ loader: false });

          this.setState({
            message: "Erreur lors de l'inscription",
            class: 'negative',
          });
          this.showMessage();
        });
    } else {
      this.setState({
        message:
          "Veuillez cocher l'autorisation de l'association des Pontonautes de mettre en ligne vos coordonnées professionnelles.",
        class: 'negative',
      });
      this.showMessage();
    }
  };

  render() {
    if (this.state.redirect) {
      return <Redirect to="/inscription-validee" />;
    } else {
      return (
        <div id="register">
          <Dimmer page active={this.state.loader}>
            <Loader />
          </Dimmer>

          <Form onSubmit={this.handleSubmit}>
            <p className="info">
              <span>*</span> champs obligatoires
            </p>

            <Form.Field>
              <Label
                ribbon
                color="blue"
                size="huge"
                content="Informations personnelles"
                style={{ marginBottom: '2.5%' }}
              />
              <Form.Group widths="equal">
                <Form.Field>
                  <Form.Input
                    required
                    id="prenom"
                    name="prenom"
                    icon="user"
                    iconPosition="left"
                    label="Prénom"
                    placeholder="Prénom"
                    type="text"
                    onChange={this.handleChange}
                    value={this.state.prenom}
                  />
                </Form.Field>
                <Form.Field>
                  <Form.Input
                    required
                    id="nom"
                    name="nom"
                    icon="user"
                    iconPosition="left"
                    label="Nom"
                    placeholder="Nom"
                    type="text"
                    onChange={this.handleChange}
                    value={this.state.nom}
                  />
                </Form.Field>
              </Form.Group>

              <Form.Group widths="equal">
                <Form.Field required>
                  <label>Je suis ...</label>
                  <br></br>
                  <Form.Field>
                    <Form.Radio
                      name="jesuis"
                      label="un commerçant/une commerçante indépendant local"
                      value="un commerçant/une commerçante indépendant local"
                      checked={
                        this.state.jesuis ===
                        'un commerçant/une commerçante indépendant local'
                      }
                      onClick={this.handleClick}
                    />
                    <br></br>
                    <Form.Radio
                      name="jesuis"
                      label="un producteur/une productrice local"
                      value="un producteur/une productrice local"
                      checked={
                        this.state.jesuis ===
                        'un producteur/une productrice local'
                      }
                      onClick={this.handleClick}
                    />
                  </Form.Field>
                </Form.Field>
                <Form.Field>
                  <Form.Input
                    required
                    id="nom_commerce"
                    name="nom_commerce"
                    icon="industry"
                    iconPosition="left"
                    label="Nom du commerce/boutique"
                    type="text"
                    onChange={this.handleChange}
                    value={this.state.nom_commerce}
                  />
                </Form.Field>
              </Form.Group>

              <Form.Group widths="2">
                <Form.Field>
                  <Form.Input
                    required
                    id="courriel"
                    name="courriel"
                    icon="mail"
                    iconPosition="left"
                    label="Courriel"
                    placeholder="nomprenom@gmail.com"
                    type="email"
                    onChange={this.handleChange}
                    value={this.state.courriel}
                  />
                </Form.Field>
              </Form.Group>

              <Form.Group widths="equal">
                <Form.Field>
                  <Form.Input
                    required
                    id="tel"
                    name="tel_fixe"
                    icon="text telephone"
                    iconPosition="left"
                    label="Téléphone fixe"
                    placeholder="02 02 02 02 02"
                    type="tel"
                    onChange={this.handleChange}
                    value={this.state.tel_fixe}
                  />
                  <small>Format : 02 02 02 02 02</small>
                </Form.Field>
                <Form.Field>
                  <Form.Input
                    required
                    id="tel"
                    name="tel_mobile"
                    icon="phone"
                    iconPosition="left"
                    label="Téléphone portable"
                    placeholder="06 06 06 06 06"
                    type="tel"
                    onChange={this.handleChange}
                    value={this.state.tel_mobile}
                  />
                  <small>Format : 06 06 06 06 06</small>
                </Form.Field>
              </Form.Group>

              <Form.Group widths="equal">
                <Form.Field>
                  <Form.Select
                    required
                    id="activite"
                    name="activite"
                    fluid
                    label="Votre activité principale"
                    options={optionsActivite}
                    placeholder={optionsActivite[0].text}
                    onChange={this.handleClick}
                  />
                </Form.Field>
                <Form.Field
                  style={{ alignSelf: 'center', textAlign: '-webkit-center' }}
                >
                  <Form.Checkbox
                    name="est_ouvert"
                    label="Êtes-vous ouvert ? (cochez si oui)"
                    checked={this.state.est_ouvert}
                    onClick={() => {
                      this.setState({
                        est_ouvert: this.state.est_ouvert ? false : true,
                      });
                    }}
                  />
                </Form.Field>
              </Form.Group>

              <Form.Field>
                <Form.Input
                  id="label"
                  name="label"
                  icon="certificate"
                  iconPosition="left"
                  label="Avez-vous un label particulier ?"
                  placeholder="Bio, Label rouge, Bienvenue à la ferme ..."
                  type="text"
                  onChange={this.handleChange}
                  value={this.state.label}
                />
              </Form.Field>
              <Form.Group widths="equal">
                <Form.TextArea
                  required
                  rows={4}
                  id="description"
                  name="description"
                  label="Description de votre boutique, produits, ce que vous désirez faire par la suite"
                  placeholder="Détaillez au maximum, ça nous aidera pour la suite"
                  type="text"
                  onChange={this.handleChange}
                  value={this.state.description}
                />
              </Form.Group>
            </Form.Field>

            <Form.Field>
              <Label
                ribbon
                color="blue"
                size="huge"
                content="Réseaux"
                style={{ marginBottom: '2.5%' }}
              />
              <Form.Group widths="equal">
                <Form.Field>
                  <Form.Input
                    id="facebook"
                    name="facebook"
                    icon="facebook"
                    iconPosition="left"
                    label="Avez-vous un Facebook ? Si oui, mettez le lien de votre profil"
                    type="text"
                    onChange={this.handleChange}
                    value={this.state.facebook}
                  />
                </Form.Field>
                <Form.Field>
                  <Form.Input
                    id="site"
                    name="site"
                    icon="linkify"
                    iconPosition="left"
                    label="Avez-vous un site internet ? Si oui, mettez le lien de votre site"
                    type="text"
                    onChange={this.handleChange}
                    value={this.state.site}
                  />
                </Form.Field>
              </Form.Group>
            </Form.Field>

            <Label
              ribbon="right"
              color="blue"
              size="huge"
              content="Nous souhaitons vous donner la possibilité d'être géolocalisé"
            />
            <Form.Field>
              <Form.Input
                required
                id="adresse"
                name="adresse"
                icon="map marker alternate"
                iconPosition="left"
                label="Adresse"
                placeholder="2B rue des carmes"
                type="text"
                onChange={this.handleChange}
                value={this.state.adresse}
              />
            </Form.Field>

            <Form.Group widths="equal">
              <Form.Field>
                <Form.Input
                  required
                  id="ville"
                  name="ville"
                  icon="map marker alternate"
                  iconPosition="left"
                  label="Ville"
                  placeholder="Pont-Audemer"
                  type="text"
                  onChange={this.handleChange}
                  value={this.state.ville}
                />
              </Form.Field>
              <Form.Field>
                <Form.Input
                  required
                  id="code_postal"
                  name="code_postal"
                  icon="map marker alternate"
                  iconPosition="left"
                  label="Code postal"
                  placeholder="27500"
                  type="text"
                  onChange={this.handleChange}
                  value={this.state.code_postal}
                />
              </Form.Field>
            </Form.Group>

            <div id="horaires">
              <Message size="large">
                <Message.Header style={{ textAlign: 'center' }}>
                  <Icon color="yellow" name="info circle" /> Informations
                  concernant les horaires
                </Message.Header>
                <Message.Content>
                  Si vous avez des horaires à cheval entre le matin et
                  l'après-midi : <br />
                  <u>Exemple :</u> 10h -> 15h, ne tenez pas compte des rubriques
                  matin/après-midi et remplissez normalement les horaires.
                  <br />
                  <br />
                  Vous n'êtes pas obligé de remplir tous les champs.
                </Message.Content>
              </Message>
              <Label content="Lundi horaires" color="blue" />
              <Form.Group widths="equal">
                <Form.Field>
                  <Form.Input
                    name="lundi_matin_debut"
                    icon="time"
                    iconPosition="left"
                    label="Matin début"
                    placeholder="8h"
                    type="text"
                    onChange={this.handleChange}
                    value={this.state.lundi_matin_debut}
                  />
                  <Form.Input
                    name="lundi_aprem_debut"
                    icon="time"
                    iconPosition="left"
                    label="Après-midi début"
                    placeholder="14h"
                    type="text"
                    onChange={this.handleChange}
                    value={this.state.lundi_aprem_debut}
                  />
                </Form.Field>
                <Form.Field>
                  <Form.Input
                    name="lundi_matin_fin"
                    icon="time"
                    iconPosition="left"
                    label="Matin fin"
                    placeholder="12h30"
                    type="text"
                    onChange={this.handleChange}
                    value={this.state.lundi_matin_fin}
                  />
                  <Form.Input
                    name="lundi_aprem_fin"
                    icon="time"
                    iconPosition="left"
                    label="Après-midi fin"
                    placeholder="17h"
                    type="text"
                    onChange={this.handleChange}
                    value={this.state.lundi_aprem_fin}
                  />
                </Form.Field>
              </Form.Group>

              <Label content="Mardi horaires" color="green" />
              <Form.Group widths="equal">
                <Form.Field>
                  <Form.Input
                    name="mardi_matin_debut"
                    icon="time"
                    iconPosition="left"
                    label="Matin début"
                    placeholder="8h"
                    type="text"
                    onChange={this.handleChange}
                    value={this.state.mardi_matin_debut}
                  />
                  <Form.Input
                    name="mardi_aprem_debut"
                    icon="time"
                    iconPosition="left"
                    label="Après-midi début"
                    placeholder="14h"
                    type="text"
                    onChange={this.handleChange}
                    value={this.state.mardi_aprem_debut}
                  />
                </Form.Field>
                <Form.Field>
                  <Form.Input
                    name="mardi_matin_fin"
                    icon="time"
                    iconPosition="left"
                    label="Matin fin"
                    placeholder="12h30"
                    type="text"
                    onChange={this.handleChange}
                    value={this.state.mardi_matin_fin}
                  />
                  <Form.Input
                    name="mardi_aprem_fin"
                    icon="time"
                    iconPosition="left"
                    label="Après-midi fin"
                    placeholder="17h"
                    type="text"
                    onChange={this.handleChange}
                    value={this.state.mardi_aprem_fin}
                  />
                </Form.Field>
              </Form.Group>

              <Label content="Mercredi horaires" color="orange" />
              <Form.Group widths="equal">
                <Form.Field>
                  <Form.Input
                    name="mercredi_matin_debut"
                    icon="time"
                    iconPosition="left"
                    label="Matin début"
                    placeholder="8h"
                    type="text"
                    onChange={this.handleChange}
                    value={this.state.mercredi_matin_debut}
                  />
                  <Form.Input
                    name="mercredi_aprem_debut"
                    icon="time"
                    iconPosition="left"
                    label="Après-midi début"
                    placeholder="14h"
                    type="text"
                    onChange={this.handleChange}
                    value={this.state.mercredi_aprem_debut}
                  />
                </Form.Field>
                <Form.Field>
                  <Form.Input
                    name="mercredi_matin_fin"
                    icon="time"
                    iconPosition="left"
                    label="Matin fin"
                    placeholder="12h30"
                    type="text"
                    onChange={this.handleChange}
                    value={this.state.mercredi_matin_fin}
                  />
                  <Form.Input
                    name="mercredi_aprem_fin"
                    icon="time"
                    iconPosition="left"
                    label="Après-midi fin"
                    placeholder="17h"
                    type="text"
                    onChange={this.handleChange}
                    value={this.state.mercredi_aprem_fin}
                  />
                </Form.Field>
              </Form.Group>

              <Label content="Jeudi horaires" color="yellow" />
              <Form.Group widths="equal">
                <Form.Field>
                  <Form.Input
                    name="jeudi_matin_debut"
                    icon="time"
                    iconPosition="left"
                    label="Matin début"
                    placeholder="8h"
                    type="text"
                    onChange={this.handleChange}
                    value={this.state.jeudi_matin_debut}
                  />
                  <Form.Input
                    name="jeudi_aprem_debut"
                    icon="time"
                    iconPosition="left"
                    label="Après-midi début"
                    placeholder="14h"
                    type="text"
                    onChange={this.handleChange}
                    value={this.state.jeudi_aprem_debut}
                  />
                </Form.Field>
                <Form.Field>
                  <Form.Input
                    name="jeudi_matin_fin"
                    icon="time"
                    iconPosition="left"
                    label="Matin fin"
                    placeholder="12h30"
                    type="text"
                    onChange={this.handleChange}
                    value={this.state.jeudi_matin_fin}
                  />
                  <Form.Input
                    name="jeudi_aprem_fin"
                    icon="time"
                    iconPosition="left"
                    label="Après-midi fin"
                    placeholder="17h"
                    type="text"
                    onChange={this.handleChange}
                    value={this.state.jeudi_aprem_fin}
                  />
                </Form.Field>
              </Form.Group>

              <Label content="Vendredi horaires" color="red" />
              <Form.Group widths="equal">
                <Form.Field>
                  <Form.Input
                    name="vendredi_matin_debut"
                    icon="time"
                    iconPosition="left"
                    label="Matin début"
                    placeholder="8h"
                    type="text"
                    onChange={this.handleChange}
                    value={this.state.vendredi_matin_debut}
                  />
                  <Form.Input
                    name="vendredi_aprem_debut"
                    icon="time"
                    iconPosition="left"
                    label="Après-midi début"
                    placeholder="14h"
                    type="text"
                    onChange={this.handleChange}
                    value={this.state.vendredi_aprem_debut}
                  />
                </Form.Field>
                <Form.Field>
                  <Form.Input
                    name="vendredi_matin_fin"
                    icon="time"
                    iconPosition="left"
                    label="Matin fin"
                    placeholder="12h30"
                    type="text"
                    onChange={this.handleChange}
                    value={this.state.vendredi_matin_fin}
                  />
                  <Form.Input
                    name="vendredi_aprem_fin"
                    icon="time"
                    iconPosition="left"
                    label="Après-midi fin"
                    placeholder="17h"
                    type="text"
                    onChange={this.handleChange}
                    value={this.state.vendredi_aprem_fin}
                  />
                </Form.Field>
              </Form.Group>

              <Label content="Samedi horaires" color="purple" />
              <Form.Group widths="equal">
                <Form.Field>
                  <Form.Input
                    name="samedi_matin_debut"
                    icon="time"
                    iconPosition="left"
                    label="Matin début"
                    placeholder="8h"
                    type="text"
                    onChange={this.handleChange}
                    value={this.state.samedi_matin_debut}
                  />
                  <Form.Input
                    name="samedi_aprem_debut"
                    icon="time"
                    iconPosition="left"
                    label="Après-midi début"
                    placeholder="14h"
                    type="text"
                    onChange={this.handleChange}
                    value={this.state.samedi_aprem_debut}
                  />
                </Form.Field>
                <Form.Field>
                  <Form.Input
                    name="samedi_matin_fin"
                    icon="time"
                    iconPosition="left"
                    label="Matin fin"
                    placeholder="12h30"
                    type="text"
                    onChange={this.handleChange}
                    value={this.state.samedi_matin_fin}
                  />
                  <Form.Input
                    name="samedi_aprem_fin"
                    icon="time"
                    iconPosition="left"
                    label="Après-midi fin"
                    placeholder="17h"
                    type="text"
                    onChange={this.handleChange}
                    value={this.state.samedi_aprem_fin}
                  />
                </Form.Field>
              </Form.Group>

              <Label content="Dimanche horaires" color="olive" />
              <Form.Group widths="equal">
                <Form.Field>
                  <Form.Input
                    name="dimanche_matin_debut"
                    icon="time"
                    iconPosition="left"
                    label="Matin début"
                    placeholder="8h"
                    type="text"
                    onChange={this.handleChange}
                    value={this.state.dimanche_matin_debut}
                  />
                  <Form.Input
                    name="dimanche_aprem_debut"
                    icon="time"
                    iconPosition="left"
                    label="Après-midi début"
                    placeholder="14h"
                    type="text"
                    onChange={this.handleChange}
                    value={this.state.dimanche_aprem_debut}
                  />
                </Form.Field>
                <Form.Field>
                  <Form.Input
                    name="dimanche_matin_fin"
                    icon="time"
                    iconPosition="left"
                    label="Matin fin"
                    placeholder="12h30"
                    type="text"
                    onChange={this.handleChange}
                    value={this.state.dimanche_matin_fin}
                  />
                  <Form.Input
                    name="dimanche_aprem_fin"
                    icon="time"
                    iconPosition="left"
                    label="Après-midi fin"
                    placeholder="17h"
                    type="text"
                    onChange={this.handleChange}
                    value={this.state.dimanche_aprem_fin}
                  />
                </Form.Field>
              </Form.Group>
              <Form.Input
                name="information_horaire"
                icon="info circle"
                iconPosition="left"
                label="Information complémentaire concernant les horaires"
                placeholder="Ouverture sur rendez-vous"
                type="text"
                onChange={this.handleChange}
                value={this.state.information_horaire}
              />
            </div>

            <Form.Group widths="equal">
              <Form.Field>
                <Label
                  color="blue"
                  size="huge"
                  ribbon
                  content="Livraison"
                  style={{ marginBottom: '2.5%' }}
                />
                <Form.Checkbox
                  name="livre"
                  label="Livrez-vous vos produits ? (cochez si oui)"
                  checked={this.state.livraison.livre}
                  onClick={() => {
                    this.setState({
                      livraison: this.state.livraison ? false : true,
                    });
                  }}
                />
                {(() => {
                  // si la livraison est cochée
                  if (this.state.livraison) {
                    return (
                      <div style={{ marginTop: '2.5%', marginLeft: '7.5%' }}>
                        <Form.Group
                          widths="equal"
                          style={{ marginBottom: '1%' }}
                        >
                          <Form.Field>
                            <Label
                              content="Comment livrez-vous ?"
                              color="yellow"
                              size="big"
                              ribbon
                              style={{ marginBottom: '2.5%' }}
                            />
                            <Form.Field>
                              {(() => {
                                let moyens = [];
                                this.state.comment.forEach((item, index) => {
                                  moyens.push(
                                    <div key={index + item.key}>
                                      <Form.Checkbox
                                        key={item.key}
                                        name="comment"
                                        label={item.value}
                                        value={item.value}
                                        checked={item.isChecked}
                                        onClick={this.handleCheckGroup}
                                      />
                                      <br></br>
                                    </div>
                                  );
                                });
                                return moyens;
                              })()}
                            </Form.Field>
                            <Form.TextArea
                              rows={3}
                              id="comment_precision"
                              name="comment_precision"
                              label="Précisions"
                              type="text"
                              onChange={this.handleChange}
                              value={this.state.comment_precision}
                            />
                          </Form.Field>
                        </Form.Group>

                        <Form.Field width="5">
                          <Form.Input
                            type="text"
                            id="panier_minimum"
                            name="panier_minimum"
                            icon="euro sign"
                            iconPosition="left"
                            label="Montant minimum pour être livré"
                            placeholder="50"
                            onChange={this.handleChange}
                            value={this.state.livraison.panier_minimum}
                          />
                        </Form.Field>
                      </div>
                    );
                  }
                })()}
              </Form.Field>
            </Form.Group>

            <Form.Group widths="equal">
              <Form.Field>
                <Label
                  color="blue"
                  size="huge"
                  ribbon
                  content="Quels sont les moyens de paiement acceptés ?"
                  style={{ marginBottom: '2.5%' }}
                />

                <Form.Field>
                  {(() => {
                    let moyens = [];
                    this.state.moyen_paiement.forEach((item, index) => {
                      moyens.push(
                        <div key={index + item.key}>
                          <Form.Checkbox
                            key={item.key}
                            name="moyen_paiement"
                            label={item.value}
                            value={item.value}
                            checked={item.isChecked}
                            onClick={this.handleCheckGroup}
                          />
                          <br></br>
                        </div>
                      );
                    });
                    return moyens;
                  })()}
                </Form.Field>
              </Form.Field>
            </Form.Group>

            <Form.Group widths="equal">
              <Form.Field>
                <Label
                  color="blue"
                  size="huge"
                  ribbon
                  content="Avez-vous un autre moyen de vente ?"
                  style={{ marginBottom: '2.5%' }}
                />

                <Form.Field>
                  {(() => {
                    let moyens = [];
                    this.state.moyen_de_vente.forEach((item, index) => {
                      moyens.push(
                        <div key={index + item.key}>
                          <Form.Checkbox
                            key={item.key}
                            name="moyen_de_vente"
                            label={item.value}
                            value={item.value}
                            checked={item.isChecked}
                            onClick={this.handleCheckGroup}
                          />
                          <br></br>
                        </div>
                      );
                    });
                    return moyens;
                  })()}
                </Form.Field>
                <Form.TextArea
                  rows={7}
                  type="text"
                  id="moyen_de_vente_precision"
                  name="moyen_de_vente_precision"
                  label="Précisions"
                  onChange={this.handleChange}
                  value={this.state.moyen_de_vente_precision}
                />
              </Form.Field>
            </Form.Group>

            <Form.Group widths="equal">
              <Form.Field>
                <Label
                  color="blue"
                  size="huge"
                  ribbon
                  content="Avez-vous une information particulière que vous souhaiteriez partager ?"
                  style={{ marginBottom: '2.5%' }}
                />
                <Form.TextArea
                  id="information_complementaire"
                  rows="4"
                  name="information_complementaire"
                  placeholder="info covid, spécialité, coup de cœur, produit de saison à mettre en valeur, ce que vous voulez développer à l'avenir ..."
                  type="text"
                  onChange={this.handleChange}
                  value={this.state.information_complementaire}
                />
              </Form.Field>
            </Form.Group>

            <Form.Checkbox
              required
              name="autorisation_pontonaute"
              label="Je donne l'autorisation à l'association des Pontonautes de mettre en ligne mes coordonnées professionnelles"
              checked={this.state.autorisation_pontonaute}
              onClick={() => {
                this.setState({
                  autorisation_pontonaute: this.state.autorisation_pontonaute
                    ? false
                    : true,
                });
              }}
            />

            <Message id="message-info" size="mini" className={this.state.class}>
              <p>{this.state.message}</p>
            </Message>

            <Button
              floated="right"
              color="blue"
              type="submit"
              size="large"
              style={{ marginTop: '2.5%', marginBottom: '5%' }}
            >
              Envoyer
            </Button>
          </Form>
        </div>
      );
    }
  }
}
