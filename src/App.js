import React from 'react';
import {
  Route,
  Switch,
  BrowserRouter as Router,
  Redirect,
} from 'react-router-dom';
import { COOKIE } from './utils/Cookie';
import { Home } from './containers/pages/Home';
import { Register } from './containers/pages/Register';

import { AdminLogin } from './containers/pages/admin/AdminLogin';
import { AdminHome } from './containers/pages/admin/AdminHome';

import { Header } from './containers/partial/Header';
import { RegisterCheck } from './components/RegisterCheck';
import { PageNotFound } from './containers/pages/404';

const ProtectedRoute = ({ ...props }) => {
  // vérification du token dans le cookie
  return COOKIE.getCookie('token').length < 200 ? (
    <Redirect to="/connexion" />
  ) : (
    <Route {...props} />
  );
};

const App = () => {
  return (
    <Router basename="/front/">
      <Header />
      <Switch>
        <Route exact path="/" component={Home} />
        <Route exact path="/inscription" component={Register} />
        <Route exact path="/connexion" component={AdminLogin} />
        <Route exact path="/inscription-validee" component={RegisterCheck} />
        <ProtectedRoute exact path="/admin" component={AdminHome} />
        <ProtectedRoute path="/admin/*" component={AdminHome} />

        <Route path="*" component={PageNotFound} />
      </Switch>
    </Router>
  );
};

export default App;
