import axios from 'axios';
import $ from 'jquery';
import { COOKIE } from './Cookie';
const __URL = require('../info').heroku; // url à modifier

/**
 * Faire des requêtes HTTP vers l'API
 */
export const API = {
  /**
   * Enregistrer une personne
   * @params {Object} data
   */
  register: data => {
    return axios.request({
      url: __URL + '/api/register',
      method: 'POST',
      data,
    });
  },
  adminLogin: (email, password) => {
    return axios.request({
      url: __URL + '/api/admin-login',
      method: 'POST',
      data: {
        email,
        password,
      },
    });
  },
  getUsers: () => {
    return axios.request({
      url: __URL + '/api/users',
      method: 'GET',
    });
  },
  /**
   * Requête synchrone de vérification du jeton
   */
  checkAuth() {
    return $.ajax({
      method: 'GET',
      url: __URL + '/api/check-auth',
      cache: false,
      headers: {
        token: COOKIE.getCookie('token'),
      },
      async: false,
    });
  },
};
