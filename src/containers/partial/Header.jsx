import React from 'react';
import { Navbar } from '../../components/Navbar';
import { Image } from 'semantic-ui-react';
export const Header = () => {
  return (
    <>
      <Image
        src={process.env.PUBLIC_URL + '/images/logo_pontonautes_decaler.png'}
        fluid
      />
      <Navbar />
    </>
  );
};
