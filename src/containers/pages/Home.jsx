import React from 'react';

/**
 * Page d'accueil avec la carte contenant les emplacements
 * de tous les utilisateurs inscrits en BDD
 */
export const Home = () => {
  return <div id="home">Carte</div>;
};
