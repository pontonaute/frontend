import React from 'react';
import { Users } from '../../../components/Users';
/**
 * Page d'accueil des admins
 * Liste de tous les utilisateurs inscrits
 */
export const AdminHome = () => {
  return <Users />;
};
