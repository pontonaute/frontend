import React from 'react';
import { Login } from '../../../components/Login';
/**
 * Page de connexion pour les admins
 */
export const AdminLogin = () => {
  return <Login />;
};
